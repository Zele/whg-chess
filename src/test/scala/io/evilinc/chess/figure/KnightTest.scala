package io.evilinc.chess.figure

import io.evilinc.chess.board.Board
import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class KnightTest extends FlatSpec with Matchers {

  "A Knight" should "be able to move in L position forward" in {
    val board = Board(init = false)

    val startSquareWhite = board.matrix(0)(1)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.KNIGHT, white = true))

    val whiteKnight = startSquareWhite.occupant.get

    val endSquareWhiteLeftL  = board.matrix(2)(0)
    val endSquareWhiteRightL = board.matrix(2)(2)

    whiteKnight canMoveTo (endSquareWhiteLeftL, board, isAttacking = false) shouldBe true
    whiteKnight canMoveTo (endSquareWhiteRightL, board, isAttacking = false) shouldBe true

    val startSquareBlack = board.matrix(7)(1)
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.KNIGHT, white = false))

    val blackKnight = startSquareBlack.occupant.get

    val endSquareBlackLeftL  = board.matrix(5)(0)
    val endSquareBlackRightL = board.matrix(5)(2)

    blackKnight canMoveTo (endSquareBlackLeftL, board, isAttacking = false) shouldBe true
    blackKnight canMoveTo (endSquareBlackRightL, board, isAttacking = false) shouldBe true
  }

  it should "be able to move in L position backward" in {
    val board            = Board(init = false)
    val startSquareWhite = board.matrix(3)(3)
    val startSquareBlack = board.matrix(3)(4)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.KNIGHT, white = true))
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.KNIGHT, white = false))

    val whiteKnight = startSquareWhite.occupant.get

    val endSquareWhiteLeftL  = board.matrix(1)(2)
    val endSquareWhiteRightL = board.matrix(1)(4)

    whiteKnight canMoveTo (endSquareWhiteLeftL, board, isAttacking = false) shouldBe true
    whiteKnight canMoveTo (endSquareWhiteRightL, board, isAttacking = false) shouldBe true

    val blackKnight = startSquareBlack.occupant.get

    val endSquareBlackLeftL  = board.matrix(5)(3)
    val endSquareBlackRightL = board.matrix(5)(5)

    blackKnight canMoveTo (endSquareBlackLeftL, board, isAttacking = false) shouldBe true
    blackKnight canMoveTo (endSquareBlackRightL, board, isAttacking = false) shouldBe true
  }
}
