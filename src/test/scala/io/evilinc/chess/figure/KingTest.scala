package io.evilinc.chess.figure

import io.evilinc.chess.Movable
import io.evilinc.chess.board.Board
import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class KingTest extends FlatSpec with Matchers with Movable {
  "A King" should "be able to move in any direction by 1 step if path is clear" in {
    val board = Board(init = false)

    val startSquareWhite = board.matrix(2)(2)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.KING, white = true))

    val whiteKing = startSquareWhite.occupant.get

    canMoveVertically(whiteKing, board, 1, isAttacking = false) shouldBe true
    canMoveVertically(whiteKing, board, -1, isAttacking = false) shouldBe true
    canMoveHorizontally(whiteKing, board, 1, isAttacking = false) shouldBe true
    canMoveHorizontally(whiteKing, board, -1, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteKing, board, 1, 1, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteKing, board, 1, -1, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteKing, board, -1, 1, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteKing, board, -1, -1, isAttacking = false) shouldBe true

    val startSquareBlack = board.matrix(3)(4)
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.KING, white = false))

    val blackKing = startSquareWhite.occupant.get

    canMoveVertically(blackKing, board, 1, isAttacking = false) shouldBe true
    canMoveVertically(blackKing, board, -1, isAttacking = false) shouldBe true
    canMoveHorizontally(blackKing, board, 1, isAttacking = false) shouldBe true
    canMoveHorizontally(blackKing, board, -1, isAttacking = false) shouldBe true
    canMoveDiagonally(blackKing, board, 1, 1, isAttacking = false) shouldBe true
    canMoveDiagonally(blackKing, board, 1, -1, isAttacking = false) shouldBe true
    canMoveDiagonally(blackKing, board, -1, 1, isAttacking = false) shouldBe true
    canMoveDiagonally(blackKing, board, -1, -1, isAttacking = false) shouldBe true
  }
}
