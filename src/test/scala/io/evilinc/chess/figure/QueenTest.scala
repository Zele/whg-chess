package io.evilinc.chess.figure

import io.evilinc.chess.Movable
import io.evilinc.chess.board.Board
import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class QueenTest extends FlatSpec with Matchers with Movable {
  "A Queen" should "be able to move in any direction if path is clear" in {
    val board = Board(init = false)

    val startSquareWhite = board.matrix(2)(2)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.QUEEN, white = true))

    val whiteQueen = startSquareWhite.occupant.get

    canMoveVertically(whiteQueen, board, 3, isAttacking = false) shouldBe true
    canMoveVertically(whiteQueen, board, -2, isAttacking = false) shouldBe true
    canMoveHorizontally(whiteQueen, board, 2, isAttacking = false) shouldBe true
    canMoveHorizontally(whiteQueen, board, -2, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteQueen, board, 2, 2, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteQueen, board, 2, -2, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteQueen, board, -2, 2, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteQueen, board, -2, -2, isAttacking = false) shouldBe true

    val startSquareBlack = board.matrix(3)(4)
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.QUEEN, white = false))

    val blackQueen = startSquareWhite.occupant.get

    canMoveVertically(blackQueen, board, 3, isAttacking = false) shouldBe true
    canMoveVertically(blackQueen, board, -2, isAttacking = false) shouldBe true
    canMoveHorizontally(blackQueen, board, 2, isAttacking = false) shouldBe true
    canMoveHorizontally(blackQueen, board, -2, isAttacking = false) shouldBe true
    canMoveDiagonally(blackQueen, board, 2, 2, isAttacking = false) shouldBe true
    canMoveDiagonally(blackQueen, board, 2, -2, isAttacking = false) shouldBe true
    canMoveDiagonally(blackQueen, board, -2, 2, isAttacking = false) shouldBe true
    canMoveDiagonally(blackQueen, board, -2, -2, isAttacking = false) shouldBe true
  }
}
