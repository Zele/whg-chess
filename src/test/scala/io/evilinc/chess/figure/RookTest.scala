package io.evilinc.chess.figure

import io.evilinc.chess.Movable
import io.evilinc.chess.board.Board
import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class RookTest extends FlatSpec with Matchers with Movable {
  "A Rook" should "be able to move horizontally in any direction if path is clear" in {
    val board = Board(init = false)

    val startSquareWhite = board.matrix(2)(2)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.ROOK, white = true))

    val whiteRook = startSquareWhite.occupant.get

    canMoveVertically(whiteRook, board, 3, isAttacking = false) shouldBe true
    canMoveVertically(whiteRook, board, -2, isAttacking = false) shouldBe true
    canMoveHorizontally(whiteRook, board, 2, isAttacking = false) shouldBe true
    canMoveHorizontally(whiteRook, board, -2, isAttacking = false) shouldBe true

    val startSquareBlack = board.matrix(3)(3)
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.ROOK, white = false))

    val blackRook = startSquareWhite.occupant.get

    canMoveVertically(blackRook, board, 3, isAttacking = false) shouldBe true
    canMoveVertically(blackRook, board, -2, isAttacking = false) shouldBe true
    canMoveHorizontally(blackRook, board, 2, isAttacking = false) shouldBe true
    canMoveHorizontally(blackRook, board, -2, isAttacking = false) shouldBe true
  }

  it should "not be able to in a direction which is blocked" in {
    val board = Board(init = false)

    val startSquareWhite = board.matrix(2)(2)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.ROOK, white = true))

    val blockSquareWhite = board.matrix(2)(4)
    blockSquareWhite.place(Figure.positionedFigureFactory(FigureType.PAWN, white = true))

    val whiteRook = startSquareWhite.occupant.get

    canMoveHorizontally(whiteRook, board, 3, isAttacking = false) shouldBe false
  }
}
