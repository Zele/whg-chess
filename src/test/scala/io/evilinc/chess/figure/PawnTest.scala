package io.evilinc.chess.figure

import io.evilinc.chess.Movable
import io.evilinc.chess.board.Board
import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class PawnTest extends FlatSpec with Matchers with Movable {
  "a Pawn" should "be able to move 2 squares forward on first move" in {
    val board            = Board(init = false)
    val startSquareWhite = board.matrix(1)(1)
    val startSquareBlack = board.matrix(6)(1)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.PAWN, white = true))
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.PAWN, white = false))

    val whitePawn = startSquareWhite.occupant.get
    val blackPawn = startSquareBlack.occupant.get

    canMoveVertically(whitePawn, board, 2, isAttacking = false) shouldBe true
    canMoveVertically(blackPawn, board, 2, isAttacking = false) shouldBe true
  }

  it should "be able to move 1 square forward" in {
    val board            = Board(init = false)
    val startSquareWhite = board.matrix(1)(1)
    val startSquareBlack = board.matrix(6)(1)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.PAWN, white = true))
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.PAWN, white = false))

    val whitePawn = startSquareWhite.occupant.get
    val blackPawn = startSquareBlack.occupant.get

    canMoveVertically(whitePawn, board, 1, isAttacking = false) shouldBe true
    canMoveVertically(blackPawn, board, 1, isAttacking = false) shouldBe true
  }

  it should "not be able to move backwards" in {
    val board            = Board(init = false)
    val startSquareWhite = board.matrix(1)(1)
    val startSquareBlack = board.matrix(6)(1)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.PAWN, white = true))
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.PAWN, white = false))

    val whitePawn = startSquareWhite.occupant.get
    val blackPawn = startSquareBlack.occupant.get

    canMoveVertically(whitePawn, board, -1, isAttacking = false) shouldBe false
    canMoveVertically(blackPawn, board, -1, isAttacking = false) shouldBe false
  }

  it should "be able to move diagonally forward by 1 square when capturing" in {
    val board            = Board(init = false)
    val startSquareWhite = board.matrix(3)(3)
    val startSquareBlack = board.matrix(6)(3)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.PAWN, white = true))
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.PAWN, white = false))

    val whitePawn = startSquareWhite.occupant.get
    val blackPawn = startSquareBlack.occupant.get

    canMoveDiagonally(whitePawn, board, 1, 1, isAttacking = true) shouldBe true
    canMoveDiagonally(blackPawn, board, 1, 1, isAttacking = true) shouldBe true
  }

  it should "not be able to move diagonally backward by 1 square when capturing" in {
    val board            = Board(init = false)
    val startSquareWhite = board.matrix(3)(3)
    val startSquareBlack = board.matrix(6)(3)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.PAWN, white = true))
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.PAWN, white = false))

    val whitePawn = startSquareWhite.occupant.get
    val blackPawn = startSquareBlack.occupant.get

    canMoveDiagonally(whitePawn, board, 1, -1, isAttacking = true) shouldBe false
    canMoveDiagonally(blackPawn, board, 1, -1, isAttacking = true) shouldBe false
  }
}
