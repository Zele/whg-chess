package io.evilinc.chess.figure

import io.evilinc.chess.Movable
import io.evilinc.chess.board.Board
import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class BishopTest extends FlatSpec with Matchers with Movable {
  "A Bishop" should "be able to move in any direction if path is clear" in {
    val board = Board(init = false)

    val startSquareWhite = board.matrix(3)(3)
    startSquareWhite.place(Figure.positionedFigureFactory(FigureType.BISHOP, white = true))

    val whiteBishop = startSquareWhite.occupant.get

    canMoveDiagonally(whiteBishop, board, 2, 2, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteBishop, board, 2, -2, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteBishop, board, -2, 2, isAttacking = false) shouldBe true
    canMoveDiagonally(whiteBishop, board, -2, -2, isAttacking = false) shouldBe true

    val startSquareBlack = board.matrix(3)(4)
    startSquareBlack.place(Figure.positionedFigureFactory(FigureType.BISHOP, white = false))

    val blackBishop = startSquareBlack.occupant.get

    canMoveDiagonally(blackBishop, board, 2, 2, isAttacking = false) shouldBe true
    canMoveDiagonally(blackBishop, board, 2, -2, isAttacking = false) shouldBe true
    canMoveDiagonally(blackBishop, board, -2, 2, isAttacking = false) shouldBe true
    canMoveDiagonally(blackBishop, board, -2, -2, isAttacking = false) shouldBe true
  }
}
