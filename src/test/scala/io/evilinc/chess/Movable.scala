package io.evilinc.chess

import io.evilinc.chess.board.Board
import io.evilinc.chess.figures.Figure

trait Movable {
  def canMoveVertically(figure: Figure, board: Board, steps: Int, isAttacking: Boolean): Boolean = {
    val correctiveFactor = if (figure.isWhite) 1 else -1

    val startSquare = figure.occupies.get
    val endSquare =
      board.matrix(startSquare.position.rank + (steps * correctiveFactor))(
        startSquare.position.file)

    figure canMoveTo (endSquare, board, isAttacking)
  }

  def canMoveHorizontally(figure: Figure,
                          board: Board,
                          steps: Int,
                          isAttacking: Boolean): Boolean = {
    val correctiveFactor = if (figure.isWhite) 1 else -1

    val startSquare = figure.occupies.get
    val endSquare =
      board.matrix(startSquare.position.rank)(
        startSquare.position.file + (steps * correctiveFactor))

    figure canMoveTo (endSquare, board, isAttacking)
  }

  def canMoveDiagonally(figure: Figure,
                        board: Board,
                        horizontalSteps: Int,
                        verticalSteps: Int,
                        isAttacking: Boolean): Boolean = {
    val correctiveFactor = if (figure.isWhite) 1 else -1

    val startSquare = figure.occupies.get

    val endRank   = startSquare.position.rank + (verticalSteps * correctiveFactor)
    val endFile   = startSquare.position.file + (horizontalSteps * correctiveFactor)
    val endSquare = board.matrix(endRank)(endFile)

    figure canMoveTo (endSquare, board, isAttacking)
  }

  def moveVertically(figure: Figure, steps: Int, board: Board): Boolean = {
    val correctiveFactor = if (figure.isWhite) 1 else -1

    val startSquare = figure.occupies.get
    val endSquare =
      board.matrix(startSquare.position.rank + (steps * correctiveFactor))(
        startSquare.position.file)

    board movePiece (startSquare, endSquare)
  }

  def moveHorizontally(figure: Figure, steps: Int, board: Board): Boolean = {
    val correctiveFactor = if (figure.isWhite) 1 else -1

    val startSquare = figure.occupies.get
    val endSquare =
      board.matrix(startSquare.position.rank)(
        startSquare.position.file + (steps * correctiveFactor))

    board movePiece (startSquare, endSquare)
  }

  def moveDiagonally(figure: Figure,
                     board: Board,
                     horizontalSteps: Int,
                     verticalSteps: Int,
                     isAttacking: Boolean): Boolean = {
    val correctiveFactor = if (figure.isWhite) 1 else -1

    val startSquare = figure.occupies.get

    val endRank   = startSquare.position.rank + (verticalSteps * correctiveFactor)
    val endFile   = startSquare.position.file + (horizontalSteps * correctiveFactor)
    val endSquare = board.matrix(endRank)(endFile)

    board movePiece (startSquare, endSquare)
  }
}
