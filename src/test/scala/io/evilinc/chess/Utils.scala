package io.evilinc.chess

object Utils {
  def buildActionString(data: List[String]): String = data.foldLeft("")(_ + _ + "\n")
}
