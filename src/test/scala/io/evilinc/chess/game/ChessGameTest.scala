package io.evilinc.chess.game

import java.io.{ByteArrayInputStream, FileInputStream}

import com.whitehatgaming.UserInputFile
import io.evilinc.chess.Awaitable
import io.evilinc.chess.player.{HumanPlayer, UserActionHandlerInputStream, UserInputAdapter}
import org.scalatest.{FlatSpec, Matchers}

class ChessGameTest extends FlatSpec with Matchers with Awaitable {
  "A ChessGame" should "be able to start and finish" in {
    val whitePlayerMoves   = "b2b4\nc2c3"
    val mockStream1        = new ByteArrayInputStream(whitePlayerMoves.getBytes())
    val whitePlayerHandler = new UserActionHandlerInputStream(mockStream1)
    val whitePlayer        = new HumanPlayer(handler = whitePlayerHandler)

    val blackPlayerMoves   = "b7b6\nc7c6"
    val mockStream2        = new ByteArrayInputStream(blackPlayerMoves.getBytes())
    val blackPlayerHandler = new UserActionHandlerInputStream(mockStream2)
    val blackPlayer        = new HumanPlayer(handler = blackPlayerHandler)

    val game = new ChessGame("Test game", whitePlayer, blackPlayer)

    game.start().await()

    game.isFinished shouldBe true
    game.isStarted shouldBe true
  }

  it should "be able to read moves from files" in {
    val testFilePath = "data/checkmate.txt"
    val mockStream   = new FileInputStream(testFilePath)
    val mockHandler  = new UserActionHandlerInputStream(mockStream)

    val whitePlayer = new HumanPlayer(handler = mockHandler)
    val blackPlayer = new HumanPlayer(handler = mockHandler)

    val game = new ChessGame("Test game", whitePlayer, blackPlayer)

    game.start().await()

    game.isFinished shouldBe true
    game.isStarted shouldBe true
  }

  it should "be able to play moves with WhiteHatGaming provided interface" in {
    val testFilePath = "data/sample-moves.txt"
    val handler      = new UserInputAdapter(new UserInputFile(testFilePath))

    val whitePlayer = new HumanPlayer(handler = handler)
    val blackPlayer = new HumanPlayer(handler = handler)

    val game = new ChessGame("Test game", whitePlayer, blackPlayer)

    game.start().await()

    game.isFinished shouldBe true
    game.isStarted shouldBe true
  }
}

object HelloWorld {
  def main(args: Array[String]): Unit = {
    val handler = new UserInputFile("data/checkmate.txt")
    println(handler.nextMove().mkString)

    val handler2 = new UserInputFile("data/checkmate.txt")
    println(handler2.nextMove().mkString)
  }
}
