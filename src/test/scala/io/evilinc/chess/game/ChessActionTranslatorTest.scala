package io.evilinc.chess.game

import io.evilinc.chess.Implicits._
import io.evilinc.chess.Types.{Action, ActionHandle}
import io.evilinc.chess.board.{Board, Position}
import io.evilinc.chess.moves.{InvalidMove, MovePiece, NoMove}
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

class ChessActionTranslatorTest extends FlatSpec with Matchers with MockFactory {
  private def buildPositionsFromAction(action: Action) = {
    val actionStringArray: Array[String] = action.split("")

    val startRank = Integer.parseInt(actionStringArray(1)) - 1
    val startFile = actionStringArray(0)

    val endRank = Integer.parseInt(actionStringArray(3)) - 1
    val endFile = actionStringArray(2)

    (Position(startRank, startFile), Position(endRank, endFile))
  }
  "A ChessActionTranslator" should "correctly decode MovePiece from valid user input" in {
    val mockGame = mock[ChessGame]
    (mockGame.whoIsOnTurn _).expects().returning(PlayerColor.WHITE)

    val testInput                            = "a2e3"
    val (testStartPosition, testEndPosition) = buildPositionsFromAction(testInput)

    val board                      = Board()
    val dummyHandler: ActionHandle = () => {}
    val actionTranslator =
      new ChessActionTranslator(dummyHandler,
                                dummyHandler,
                                dummyHandler,
                                dummyHandler,
                                board,
                                mockGame)

    val move = actionTranslator.translate(testInput).asInstanceOf[MovePiece]

    move shouldBe a[MovePiece]

    move.startPosition should equal(testStartPosition)
    move.endPosition should equal(testEndPosition)
  }

  it should "return invalid move if invalid user input is provided" in {
    val mockGame = mock[ChessGame]

    val testInput = "bad2"

    val board                      = Board()
    val dummyHandler: ActionHandle = () => {}
    val actionTranslator =
      new ChessActionTranslator(dummyHandler,
                                dummyHandler,
                                dummyHandler,
                                dummyHandler,
                                board,
                                mockGame)

    val move = actionTranslator.translate(testInput)

    move shouldBe a[InvalidMove]
  }

  it should "return no move if no user (blank) input is provided" in {
    val mockGame = mock[ChessGame]

    val testInput = ""

    val board                      = Board()
    val dummyHandler: ActionHandle = () => {}
    val actionTranslator =
      new ChessActionTranslator(dummyHandler,
                                dummyHandler,
                                dummyHandler,
                                dummyHandler,
                                board,
                                mockGame)

    val move = actionTranslator.translate(testInput)

    move shouldBe a[NoMove]
  }
}
