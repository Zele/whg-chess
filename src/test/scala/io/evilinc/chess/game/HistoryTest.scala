package io.evilinc.chess.game

import io.evilinc.chess.moves.Move
import io.evilinc.chess.player.Player
import org.scalamock.scalatest.MockFactory
import org.scalatest.FlatSpec
import org.scalatest.Matchers._

class HistoryTest extends FlatSpec with MockFactory {
  "A History" should "save a player's move" in {
    val moveMock   = mock[Move]
    val playerMock = mock[Player]

    val history = new History()

    history.archive(moveMock, playerMock)

    history.getMovesFor(playerMock) should have size 1
  }

  "A History" should "throw NoSuchElement exception if player entry does not exist" in {
    val player = mock[Player]

    val history = new History()

    an[NoSuchElementException] should be thrownBy history.getMovesFor(player)
  }
}
