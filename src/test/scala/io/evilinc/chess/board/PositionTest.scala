package io.evilinc.chess.board

import io.evilinc.chess.Implicits._
import org.scalatest.{FlatSpec, Matchers}

class PositionTest extends FlatSpec with Matchers {
  "A Position" should "be created with correct arguments as integers" in {
    val rank = 2
    val file = 3

    val p = new Position(rank, file)

    p.rank should equal(rank)
    p.file should equal(file)
  }

  it should "be created with correct arguments where file is integer and rank is string" in {
    val rank                 = 2
    val fileAsString: String = "c"
    val fileAsInt: Int       = "c"

    val p = new Position(rank, fileAsString)

    p.rank should equal(rank)
    p.file should equal(fileAsInt)
  }

  it should "throw InvalidArgumentException when rank and/or file is outside of bounds" in {
    an[IllegalArgumentException] should be thrownBy Position(-1, 3)

    an[IllegalArgumentException] should be thrownBy Position(0, 9)

    an[IllegalArgumentException] should be thrownBy Position(-1, 15)

    an[IllegalArgumentException] should be thrownBy Position(2, "t")

    an[IllegalArgumentException] should be thrownBy Position(12, "f")

    an[IllegalArgumentException] should be thrownBy Position(12, "y")
  }
}
