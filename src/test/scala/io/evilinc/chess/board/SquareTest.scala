package io.evilinc.chess.board

import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class SquareTest extends FlatSpec with Matchers {

  "A Square" should "be created with an empty slot" in {
    val square = Square(2, 3)

    square.isOccupied shouldBe false
  }

  it should "be able hold a figure" in {
    val square     = Square(2, 3)
    val white      = true
    val figureType = FigureType.QUEEN

    square.place(Figure.positionedFigureFactory(figureType, white))

    square.isOccupied shouldBe true

    val occupant = square.occupant.get

    occupant.isWhite should equal(white)
    occupant.identifier should equal(figureType)
    occupant.occupies should equal(Some(square))
  }

  it should "be able to free it's slot" in {
    val square     = Square(2, 3)
    val white      = true
    val figureType = FigureType.QUEEN

    square.place(Figure.positionedFigureFactory(figureType, white))

    square.free()

    square.occupant should be(None)
  }

  it should "throw an IllegalArgumentException if invalid position is specified" in {
    an[IllegalArgumentException] should be thrownBy Square(9, -1)
  }
}
