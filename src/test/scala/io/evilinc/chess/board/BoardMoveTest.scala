package io.evilinc.chess.board

import io.evilinc.chess.Movable
import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class BoardMoveTest extends FlatSpec with Matchers with Movable {
  "A Board" should "correctly move a pawn forward by 2 steps" in {
    val board = Board()

    val startSquare = board.matrix(1)(1)
    val whitePawn   = startSquare.occupant.get

    moveVertically(whitePawn, 2, board) shouldBe true

    moveVertically(whitePawn, 2, board) shouldBe false
  }

  it should "not move a pawn forward by 2 steps if it is not first turn" in {
    val board = Board()

    val startSquare = board.matrix(1)(1)
    val whitePawn   = startSquare.occupant.get

    moveVertically(whitePawn, 1, board) shouldBe true

    moveVertically(whitePawn, 2, board) shouldBe false
  }

  it should "correctly move a pawn forward by 1 step" in {
    val board = Board()

    val startSquare = board.matrix(1)(1)
    val whitePawn   = startSquare.occupant.get

    moveVertically(whitePawn, 1, board) shouldBe true
  }

  it should "correctly move a pawn and kill an enemy figure" in {
    val board = Board(init = false)

    val square1 = board.matrix(1)(1)
    val square2 = board.matrix(2)(2)
    val square3 = board.matrix(0)(5)

    square1.place(Figure.positionedFigureFactory(FigureType.PAWN, white = true))
    square2.place(Figure.positionedFigureFactory(FigureType.BISHOP, white = false))
    square3.place(Figure.positionedFigureFactory(FigureType.KING, white = false))

    val blackFigure = square2.occupant.get

    board.movePiece(square1, square2) shouldBe true

    board.getGraveyard should have length 1

    board.getGraveyard.head should equal(blackFigure)
  }

  it should "successfully calculate opponents King is under attack after threatening move by white player" in {
    val board = Board(init = false)

    val square1 = board.matrix(0)(0)
    val square2 = board.matrix(7)(4)
    val square3 = board.matrix(0)(6)

    square1.place(Figure.positionedFigureFactory(FigureType.ROOK, white = true))
    square2.place(Figure.positionedFigureFactory(FigureType.KING, white = false))
    square3.place(Figure.positionedFigureFactory(FigureType.KING, white = true))

    val whiteRook = square1.occupant.get

    moveHorizontally(whiteRook, 4, board) shouldBe true

    board.display(System.out)

    board.isInCheck shouldBe true
  }

  it should "successfully calculate opponents King is under attack after threatening move by black player" in {
    val board = Board(init = false)

    val blackSquare1 = board.matrix(7)(0)
    val blackSquare2 = board.matrix(0)(1)
    val blackSquare3 = board.matrix(7)(5)

    blackSquare1.place(Figure.positionedFigureFactory(FigureType.QUEEN, white = false))
    blackSquare2.place(Figure.positionedFigureFactory(FigureType.KING, white = true))
    blackSquare3.place(Figure.positionedFigureFactory(FigureType.KING, white = false))

    val blackQueen = blackSquare1.occupant.get

    moveHorizontally(blackQueen, -1, board) shouldBe true

    board.isInCheck shouldBe true
  }

  it should "revert a move, given that the player in check, if it doesn't escape check position" in {
    val board = Board(init = false)

    val square1 = board.matrix(0)(0)
    val square2 = board.matrix(7)(4)
    val square3 = board.matrix(0)(6)

    square1.place(Figure.positionedFigureFactory(FigureType.ROOK, white = true))
    square2.place(Figure.positionedFigureFactory(FigureType.KING, white = false))
    square3.place(Figure.positionedFigureFactory(FigureType.KING, white = true))

    val whiteRook = square1.occupant.get
    val blackKing = square2.occupant.get

    moveHorizontally(whiteRook, 4, board) shouldBe true

    board.isInCheck shouldBe true

    moveVertically(blackKing, 1, board) shouldBe false

    blackKing.occupies shouldBe Some(square2)

    board.isInCheck shouldBe true
  }

  it should "allow a move, given that the player in check, if it escapes check position" in {
    val board = Board(init = false)

    val square1 = board.matrix(0)(0)
    val square2 = board.matrix(7)(4)
    val square3 = board.matrix(0)(6)

    square1.place(Figure.positionedFigureFactory(FigureType.ROOK, white = true))
    square2.place(Figure.positionedFigureFactory(FigureType.KING, white = false))
    square3.place(Figure.positionedFigureFactory(FigureType.KING, white = true))

    val whiteRook = square1.occupant.get
    val blackKing = square2.occupant.get

    moveHorizontally(whiteRook, 4, board) shouldBe true

    board.isInCheck shouldBe true

    board.display(System.out)

    moveHorizontally(blackKing, 1, board) shouldBe true

    board.isInCheck shouldBe false

    board.display(System.out)
  }

  it should "successfully calculate opponents King is not under attack after non-threatening move" in {
    val board = Board(init = false)

    val square1 = board.matrix(0)(0)
    val square2 = board.matrix(7)(4)
    val square3 = board.matrix(2)(3)

    square1.place(Figure.positionedFigureFactory(FigureType.ROOK, white = true))
    square2.place(Figure.positionedFigureFactory(FigureType.KING, white = false))
    square3.place(Figure.positionedFigureFactory(FigureType.KING, white = true))

    val whiteRook = square1.occupant.get

    moveVertically(whiteRook, 4, board) shouldBe true

    board.isInCheck shouldBe false
  }
}
