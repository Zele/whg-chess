package io.evilinc.chess.board

import io.evilinc.chess.Movable
import io.evilinc.chess.figures.Figure.FigureType
import org.scalatest.{FlatSpec, Matchers}

class BoardTest extends FlatSpec with Matchers with Movable {

  private def validateInitialPawnOrder(board: Board): Unit = {
    val squares = board.matrix

    def validatePawnsInRow(row: Array[Square], white: Boolean): Unit =
      row.foreach(square => {
        val pawn = square.occupant.get

        pawn.isWhite shouldBe white
        pawn.identifier should be(FigureType.PAWN)
        pawn.occupies should be(Some(square))
      })

    val whitePawnRow = squares(1)
    validatePawnsInRow(whitePawnRow, white = true)

    val blackPawnRow = squares(6)
    validatePawnsInRow(blackPawnRow, white = false)
  }

  private def validateEmptySquares(board: Board) =
    board.matrix.slice(2, 6).map(verifySquaresAreEmpty)

  private def verifySquaresAreEmpty(squares: Array[Square]): Unit =
    squares.foreach(_.occupant.isEmpty shouldBe true)

  private def validateInitialKingOrder(board: Board) = {
    val board = Board()

    val whiteKing = getPiece(board, 0, 3)
    whiteKing.identifier should equal(FigureType.KING)
    whiteKing.isWhite shouldBe true

    val blackKing = getPiece(board, 7, 3)
    blackKing.identifier should equal(FigureType.KING)
    blackKing.isWhite shouldBe false
  }

  private def validateInitialQueenOrder(board: Board) = {
    val board = Board()

    val whiteQueen = getPiece(board, 0, 4)
    whiteQueen.identifier should equal(FigureType.QUEEN)
    whiteQueen.isWhite shouldBe true

    val blackQueen = getPiece(board, 7, 4)
    blackQueen.identifier should equal(FigureType.QUEEN)
    blackQueen.isWhite shouldBe false
  }

  private def getPiece(board: Board, rank: Int, file: Int) =
    board.matrix(rank)(file).occupant.get

  "A Board" should "be initialized with pieces in correct order when created" in {
    val board = Board()

    validateEmptySquares(board)
    validateInitialPawnOrder(board)
    validateInitialKingOrder(board)
    validateInitialQueenOrder(board)
  }

  it should "should correctly calculate path between b2 and b4 is empty" in {
    val board = Board()
    val from  = board.matrix(1)(1)
    val to    = board.matrix(3)(1)

    board.isPathClear(from, to, attacking = false) shouldBe true
  }

  it should "should correctly calculate path between b2 and d4 is empty" in {
    val board = Board()
    val from  = board.matrix(1)(1)
    val to    = board.matrix(3)(3)

    board.isPathClear(from, to, attacking = false) shouldBe true
  }

  it should "should correctly calculate path between g2 and e4 is empty" in {
    val board = Board()
    val from  = board.matrix(6)(1)
    val to    = board.matrix(4)(3)

    board.isPathClear(from, to, attacking = false) shouldBe true
  }

  it should "should correctly calculate path between d7 and d5 is empty" in {
    val board = Board()
    val from  = board.matrix(6)(3)
    val to    = board.matrix(4)(3)

    board.isPathClear(from, to, attacking = false) shouldBe true
  }

  it should "should correctly calculate path between e1 and e5 is blocked" in {
    val board = Board()
    val from  = board.matrix(0)(4)
    val to    = board.matrix(4)(4)

    board.isPathClear(from, to, attacking = false) shouldBe false
  }

  it should "should correctly calculate path between e8 and c6 is blocked" in {
    val board = Board()
    val from  = board.matrix(7)(4)
    val to    = board.matrix(5)(4)

    board.isPathClear(from, to, attacking = false) shouldBe false
  }

  it should "should correctly calculate path between e2 and e7 is valid when attacking" in {
    val board = Board()
    val from  = board.matrix(1)(4)
    val to    = board.matrix(6)(4)

    board.isPathClear(from, to, attacking = true) shouldBe true
  }

  it should "should correctly calculate path between e7 and e2 is valid when attacking" in {
    val board = Board()
    val from  = board.matrix(6)(4)
    val to    = board.matrix(1)(4)

    board.isPathClear(from, to, attacking = true) shouldBe true
  }

  it should "be able to get square by position" in {
    val board = Board()

    val position = Position(1, 1)

    val square = board.getSquare(position)

    square.position should equal(position)
  }
}
