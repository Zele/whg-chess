package io.evilinc.chess.player

import java.io.ByteArrayInputStream

import io.evilinc.chess.Utils
import org.scalatest.{FlatSpec, Matchers}

class UserActionHandlerInputStreamTest extends FlatSpec with Matchers {
  "An UserActionInputStream" should "return next action from input stream if it exists" in {
    val testDataString    = "e2a4"
    val mockInputStream   = new ByteArrayInputStream(testDataString.getBytes())
    val userActionHandler = new UserActionHandlerInputStream(mockInputStream)

    val action = userActionHandler.requestAction()

    action should equal(testDataString)
  }

  it should "return all actions from input stream" in {

    val testData1       = "e2a4"
    val testData2       = "a2e4"
    val testData3       = "c3d5"
    val testString      = Utils.buildActionString(List(testData1, testData2, testData3))
    val mockInputStream = new ByteArrayInputStream(testString.getBytes())
    val userAction      = new UserActionHandlerInputStream(mockInputStream)

    val action1 = userAction.requestAction()
    val action2 = userAction.requestAction()
    val action3 = userAction.requestAction()

    action1 should equal(testData1)
    action2 should equal(testData2)
    action3 should equal(testData3)
  }

  it should "return empty action" in {
    val testDataString  = "e2a4"
    val mockInputStream = new ByteArrayInputStream(testDataString.getBytes())
    val userAction      = new UserActionHandlerInputStream(mockInputStream)

    userAction.requestAction()
    val emptyAction = userAction.requestAction()

    emptyAction shouldBe empty
  }

  it should "throw IllegalArgumentException if no InputStream is provided" in {
    an[IllegalArgumentException] should be thrownBy new UserActionHandlerInputStream(null)
  }
}
