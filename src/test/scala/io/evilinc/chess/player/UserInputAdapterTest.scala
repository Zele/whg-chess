package io.evilinc.chess.player

import com.whitehatgaming.UserInput
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

class UserInputAdapterTest extends FlatSpec with Matchers with MockFactory {

  "An UserInputAdapter" should "return next action from input stream if it exists" in {
    val userInputMock = mock[UserInput]
    val testData      = Array(3, 2, 3, 4)
    (userInputMock.nextMove _).expects().returning(testData)

    val userInputHandler = new UserInputAdapter(userInputMock)

    val action = userInputHandler.requestAction()

    val expectedData = "d3d5"

    action should equal(expectedData)
  }

  it should "return empty action from input stream if it exists" in {
    val userInputMock = mock[UserInput]
    (userInputMock.nextMove _).expects().returning(null)

    val userInputHandler = new UserInputAdapter(userInputMock)

    val action = userInputHandler.requestAction()

    action shouldBe empty
  }

  it should "throw IllegalArgumentException if no InputStream is provided" in {
    an[IllegalArgumentException] should be thrownBy new UserInputAdapter(null)
  }
}
