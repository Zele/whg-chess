package io.evilinc.chess.player

import io.evilinc.chess.Awaitable
import org.scalamock.scalatest.MockFactory
import org.scalatest.{FlatSpec, Matchers}

class HumanPlayerTest extends FlatSpec with Matchers with Awaitable with MockFactory {
  "A HumanPlayer" should "return next action when play is invoked" in {
    val testAction        = "e3e5"
    val userActionHandler = mock[UserActionHandler]
    (userActionHandler.requestAction _).expects().returning(testAction)

    val player = new HumanPlayer(handler = userActionHandler)
    val action = player.play().await()

    action should equal(testAction)
  }

  it should "return empty action when there are no more actions from handler" in {
    val userActionHandler = mock[UserActionHandler]
    (userActionHandler.requestAction _).expects().returning("")

    val player      = new HumanPlayer(handler = userActionHandler)
    val emptyAction = player.play().await()

    emptyAction shouldBe empty
  }
}
