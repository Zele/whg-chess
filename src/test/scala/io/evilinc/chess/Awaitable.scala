package io.evilinc.chess

import scala.concurrent.duration._
import scala.concurrent.{Await, Future}
import scala.language.postfixOps

trait Awaitable {
  implicit class AwaitFuture[T](future: Future[T]) {
    def await(timeout: Duration = 120 seconds): T = {
      Await.result(future, timeout)
    }
  }
}
