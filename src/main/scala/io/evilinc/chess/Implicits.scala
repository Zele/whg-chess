package io.evilinc.chess

object Implicits {
  implicit def file2int(c: String): Int =
    c.toLowerCase() match {
      case "a" => 0
      case "b" => 1
      case "c" => 2
      case "d" => 3
      case "e" => 4
      case "f" => 5
      case "g" => 6
      case "h" => 7
      case invalid =>
        throw new IllegalArgumentException(
          s"File must be between 'a' and 'h' or 0 and 7! Found: $invalid")
    }

  implicit def int2file(i: Int): String =
    i match {
      case 0 => "a"
      case 1 => "b"
      case 2 => "c"
      case 3 => "d"
      case 4 => "e"
      case 5 => "f"
      case 6 => "g"
      case 7 => "h"
      case invalid =>
        throw new IllegalArgumentException(
          s"File must be between 'a' and 'h' or 0 and 7! Found: $invalid")
    }
}
