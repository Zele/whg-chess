package io.evilinc.chess

import io.evilinc.chess.board.Square

object Types {
  type Action       = String
  type SquareMatrix = Array[Array[Square]]
  type ActionHandle = () => Unit
}
