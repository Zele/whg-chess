package io.evilinc.chess.game

import io.evilinc.chess.Types.Action
import io.evilinc.chess.moves.Move

trait ActionTranslator {
  def translate(action: Action): Move
}
