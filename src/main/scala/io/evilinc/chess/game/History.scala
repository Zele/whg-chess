package io.evilinc.chess.game

import io.evilinc.chess.moves.Move
import io.evilinc.chess.player.Player

class History {
  private var history: Map[Player, List[Move]] = Map()

  def archive(move: Move, player: Player): Unit = {
    if (history.contains(player))
      history = history + (player -> (history(player) :+ move))
    else
      history = history + (player -> List(move))
  }

  def getMovesFor(player: Player): List[Move] = history(player)
}
