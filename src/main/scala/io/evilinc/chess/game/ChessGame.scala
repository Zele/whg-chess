package io.evilinc.chess.game

import java.util.UUID

import io.evilinc.chess.Logging
import io.evilinc.chess.board.Board
import io.evilinc.chess.game.PlayerColor.PlayerColor
import io.evilinc.chess.player.Player

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class ChessGame(name: String = s"New Chess game - ${UUID.randomUUID()}",
                whitePlayer: Player,
                blackPlayer: Player)
    extends Logging {

  val id: UUID                   = UUID.randomUUID()
  private var started            = false
  private var finished           = false
  private var currentPlayerColor = PlayerColor.WHITE
  private val board              = Board()
  private val history            = new History()

  def whoIsOnTurn: PlayerColor = currentPlayerColor
  def isStarted: Boolean       = started
  def isFinished: Boolean      = finished

  private val invalidMoveHandle = () => {
    val playerColor = getCurrentPlayerColor.capitalize

    log.info(
      s"$playerColor Player has made an invalid move! Requesting another move from player...")
  }
  private val successfulMoveHandle = () => {
    board.display(System.out)
    flipPlayerTurn()
  }
  private val endGameHandle = () => end()
  private val noMoreMovesHandle = () => {
    log.info(s"$currentPlayer has no more moves. Ending game...")
    end()
  }

  private val actionTranslator =
    new ChessActionTranslator(endGameHandle,
                              successfulMoveHandle,
                              invalidMoveHandle,
                              noMoreMovesHandle,
                              board,
                              this)

  def currentPlayer: Player =
    if (currentPlayerColor == PlayerColor.WHITE) whitePlayer else blackPlayer
  def getCurrentPlayerColor: String =
    if (currentPlayerColor == PlayerColor.BLACK) "white" else "black"

  def flipPlayerTurn(): Unit = currentPlayerColor = !currentPlayerColor

  private def nextTurn(): Future[Unit] = {
    val futurePlayerMove =
      currentPlayer.play()

    futurePlayerMove.flatMap(playerMove => {
      val move = actionTranslator.translate(playerMove)

      move.execute()

      history.archive(move, currentPlayer)

      if (!finished)
        nextTurn()
      else
        Future.successful()
    })
  }

  def start(): Future[Unit] = {
    log.info(s"$this has started!")

    started = true

    nextTurn()
      .map(_ => {
        log.info(s"$this has finished!")
      })
  }

  def end(): Unit = finished = true

  override def toString: String = s"Game[name=$name, id=$id]"
}

object PlayerColor extends Enumeration {
  type PlayerColor = Boolean
  val WHITE = true
  val BLACK = false
}
