package io.evilinc.chess.game

import io.evilinc.chess.Implicits._
import io.evilinc.chess.Logging
import io.evilinc.chess.Types.{Action, ActionHandle}
import io.evilinc.chess.board.{Board, Position}
import io.evilinc.chess.moves.{InvalidMove, Move, MovePiece, NoMove}

class ChessActionTranslator(
    endGameHandle: ActionHandle,
    successfulMoveHandle: ActionHandle,
    invalidMoveHandle: ActionHandle,
    noMoreMovesHandle: ActionHandle,
    board: Board,
    game: ChessGame
) extends ActionTranslator
    with Logging {

  override def translate(action: Action): Move = {
    if (action.isBlank)
      return new NoMove(noMoreMovesHandle)

    val actionCharArray: Array[String] = action.split("")

    if (actionCharArray.length != 4)
      return new InvalidMove(invalidMoveHandle)

    try {
      val startRank     = Integer.parseInt(actionCharArray(1)) - 1
      val startFile     = actionCharArray(0)
      val startPosition = Position(startRank, startFile)

      val endRank     = Integer.parseInt(actionCharArray(3)) - 1
      val endFile     = actionCharArray(2)
      val endPosition = Position(endRank, endFile)

      val startSquare = board.getSquare(startPosition)
      if (startSquare.isEmpty) {
        log.warn(s"Error moving figure from $startSquare! Square is empty.")

        return new InvalidMove(invalidMoveHandle)
      }
      val figure = startSquare.occupant.get

      if (figure.isWhite != game.whoIsOnTurn) {
        log.warn(s"Error moving $figure from $startSquare! Cannot move opponent's figures.")

        return new InvalidMove(invalidMoveHandle)
      }

      new MovePiece(startPosition, endPosition, board, successfulMoveHandle, invalidMoveHandle)
    } catch {
      case ex: Exception =>
        log.warn(s"Exception occurred while translating action: $action! Exception: ${ex.getCause}")
        new InvalidMove(invalidMoveHandle)
    }
  }
}
