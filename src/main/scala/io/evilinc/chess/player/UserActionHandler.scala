package io.evilinc.chess.player

import io.evilinc.chess.Types.Action

trait UserActionHandler {
  def requestAction(): Action
}
