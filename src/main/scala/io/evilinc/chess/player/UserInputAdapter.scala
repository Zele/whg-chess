package io.evilinc.chess.player

import com.whitehatgaming.UserInput
import io.evilinc.chess.Implicits._
import io.evilinc.chess.Logging
import io.evilinc.chess.Types.Action

class UserInputAdapter(userInput: UserInput) extends UserActionHandler with Logging {
  require(userInput != null, "UserInput cannot be null!")

  private val noAction: Action = ""

  override def requestAction(): Action = {
    val positions = userInput.nextMove()
    if (positions == null || positions.isEmpty)
      noAction
    else {
      try {
        val startRank         = positions(1) + 1
        val startFile: String = positions(0)
        val endRank           = positions(3) + 1
        val endFile: String   = positions(2)

        Array(startFile, startRank, endFile, endRank).mkString
      } catch {
        case ex: Exception =>
          log.error(s"Error decoding values from action! Exception: ${ex.getMessage}")
          noAction
      }
    }
  }
}
