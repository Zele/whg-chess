package io.evilinc.chess.player

import scala.concurrent.Future

trait AIEngine {
  def calculateNextMove(): Future[String]
}
