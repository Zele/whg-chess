package io.evilinc.chess.player

import java.util.UUID

import io.evilinc.chess.Types.Action

import scala.concurrent.Future

class ComputerPlayer(aiEngine: AIEngine) extends Player {
  override val id: UUID = UUID.randomUUID()

  override def toString = s"ComputerPlayer(id: ${id})"

  override def play(): Future[Action] = aiEngine.calculateNextMove()
}
