package io.evilinc.chess.player

import java.io.{BufferedReader, InputStream, InputStreamReader}

import io.evilinc.chess.Types.Action

class UserActionHandlerInputStream(is: InputStream) extends UserActionHandler with AutoCloseable {
  require(is != null, "InputStream cannot be null!")

  private val inputStreamReader = new InputStreamReader(is)
  private val bufferedReader    = new BufferedReader(inputStreamReader)

  override def requestAction(): Action = {
    val action = bufferedReader.readLine()

    if (action == null || action.isBlank)
      ""
    else
      action
  }

  override def close(): Unit = {
    bufferedReader.close()
    inputStreamReader.close()
  }
}
