package io.evilinc.chess.player

import java.util.UUID

import io.evilinc.chess.Logging
import io.evilinc.chess.Types.Action

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class HumanPlayer(maybePerson: Option[Person] = None, handler: UserActionHandler)
    extends Player
    with Logging {

  override val id: UUID = maybePerson.map(person => person.id).getOrElse(UUID.randomUUID())

  override def toString = s"HumanPlayer(id: ${id})"

  override def play(): Future[Action] = Future {
    log.info(s"${this} is playing a move...")
    val action = handler.requestAction()
    log.info(s"$this: $action")
    action
  }
}
