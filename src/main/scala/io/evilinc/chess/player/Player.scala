package io.evilinc.chess.player

import java.util.UUID

import io.evilinc.chess.Types.Action

import scala.concurrent.Future

trait Player {
  val id: UUID

  def play(): Future[Action]
}
