package io.evilinc.chess.player

import java.util.{Date, UUID}

case class Person(id: UUID,
                  username: String,
                  firstName: String,
                  lastName: String,
                  email: String,
                  born: Date)
