package io.evilinc.chess

import java.io.FileInputStream

import scala.language.postfixOps

import io.evilinc.chess.game.ChessGame
import io.evilinc.chess.player.{HumanPlayer, UserActionHandlerInputStream}

import scala.concurrent.duration._

import scala.concurrent.Await

object Main extends Logging {
  def main(args: Array[String]): Unit = {
    if (args.length < 1)
      throw new IllegalArgumentException("File path must be provided as command-line argument!")
    val gameName = if (args.length > 1) args(1) else "New game"
    val moveFilePath = args(0)
    val fileStream   = new FileInputStream(moveFilePath)
    val handler      = new UserActionHandlerInputStream(fileStream)

    val whitePlayer = new HumanPlayer(handler = handler)
    val blackPlayer = new HumanPlayer(handler = handler)

    val game = new ChessGame(gameName, whitePlayer, blackPlayer)

    val maximumMoveTime = 120 seconds

    Await.result(game.start(), maximumMoveTime)
  }
}
