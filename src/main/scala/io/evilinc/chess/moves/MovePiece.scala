package io.evilinc.chess.moves

import io.evilinc.chess.Types.ActionHandle
import io.evilinc.chess.board.{Board, Position}

class MovePiece(val startPosition: Position,
                val endPosition: Position,
                board: Board,
                successfulMoveHandle: ActionHandle,
                invalidMoveHandle: ActionHandle)
    extends Move {

  override def execute(): Unit = {
    val startSquare = board.getSquare(startPosition)
    val endSquare   = board.getSquare(endPosition)
    if (board.movePiece(startSquare, endSquare))
      successfulMoveHandle()
    else
      invalidMoveHandle()
  }
}
