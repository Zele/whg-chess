package io.evilinc.chess.moves

trait Move {
  def execute(): Unit
}
