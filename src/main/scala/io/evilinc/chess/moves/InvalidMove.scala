package io.evilinc.chess.moves

import io.evilinc.chess.Types.ActionHandle

class InvalidMove(endGameHandle: ActionHandle) extends Move {
  override def execute(): Unit = endGameHandle()
}
