package io.evilinc.chess.moves

import io.evilinc.chess.Types.ActionHandle

class NoMove(noMoveHandle: ActionHandle) extends Move {
  override def execute(): Unit = noMoveHandle()
}
