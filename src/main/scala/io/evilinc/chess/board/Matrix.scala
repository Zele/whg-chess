package io.evilinc.chess.board

import scala.reflect.ClassTag

class Matrix[T: ClassTag](N: Int, M: Int) {
  val matrix: Array[Array[T]] = Array.ofDim[T](N, M)

  private def init(positionFunction: (Int, Int) => T): Unit = {
    for (i <- 0 until N;
         j <- 0 until M) matrix(i)(j) = positionFunction(i, j)
  }
}

object Matrix {
  def apply[T: ClassTag](N: Int, M: Int, positionFunction: (Int, Int) => T): Matrix[T] = {
    val matrix = new Matrix[T](N, M)
    matrix.init(positionFunction)
    matrix
  }
}
