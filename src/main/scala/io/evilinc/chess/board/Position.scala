package io.evilinc.chess.board

import scala.language.implicitConversions

case class Position(rank: Int, file: Int) {
  require(rank >= 0 && rank < 8, "Rank must be between 0 and 7 inclusively!")
  require(file >= 0 && file < 8, "File must be between 0 and 7, or 'a' and 'h' inclusively!")

  override def toString: String = s"[${rank}, ${file}]"
}
