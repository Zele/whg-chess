package io.evilinc.chess.board

import java.io.OutputStream

import io.evilinc.chess.Logging
import io.evilinc.chess.Types.SquareMatrix
import io.evilinc.chess.figures.Figure
import io.evilinc.chess.figures.Figure.FigureType
import io.evilinc.chess.figures.Figure.FigureType.FigureType

import scala.util.control.Breaks._

class Board private (
    squareMatrix: Matrix[Square],
    os: OutputStream = System.out
) extends Logging {

  def matrix: SquareMatrix           = squareMatrix.matrix
  def getSquare(p: Position): Square = matrix(p.rank)(p.file)

  private var graveyard: List[Figure] = List()
  def getGraveyard: List[Figure]      = graveyard

  private var isCheck = false
  private def check(): Unit = {
    isCheck = true
    notifyCheck()
  }
  private def notifyCheck(): Unit = {
    val newLine = "\n"
    val msg = newLine + "CHECK" + newLine
    os.write(msg.getBytes())
  }
  private def clearCheck(): Unit = isCheck = false
  def isInCheck: Boolean         = isCheck

  private def isKingUnderAttack(isWhitePlayer: Boolean): Boolean = {
    val color = (if (isWhitePlayer) "white" else "black").capitalize
    log.info(s"Checking is $color King under attack...")

    val isAttackingFigure: Figure => Boolean = (figure: Figure) => figure.isWhite != isWhitePlayer
    val attackingFigures: Seq[Figure] =
      matrix.flatten.flatMap(_.occupant).filter(isAttackingFigure)

    attackingFigures.exists(figure => {
      val isOpponentKing =
        (figure: Figure) => figure.isWhite == isWhitePlayer && figure.identifier == FigureType.KING
      val kingSquare: Square =
        matrix.flatten.find(square => square.occupant.exists(isOpponentKing)).get
      val king = kingSquare.occupant.get

      val isKingUnderAttack =
        figure.canMoveTo(kingSquare, this, isAttacking = true)

      if (isKingUnderAttack) {
        log.info(s"$king is under attack by $figure!")
      }

      isKingUnderAttack
    })
  }

  def movePiece(from: Square, to: Square): Boolean = {
    from.occupant
      .map(attackingFigure => {
        log.info(s"Moving piece from $from to $to...")

        to.occupant
          .map(attackedFigure => tryAttack(attackingFigure, attackedFigure, from, to))
          .getOrElse(tryMove(attackingFigure, from, to))
      }) getOrElse {
      log.warn(s"$from does not contain a figure!")
      false
    }
  }

  private def tryMove(figure: Figure, from: Square, to: Square): Boolean = {
    log.info(s"Checking if $figure can move to $to...")
    if (figure canMoveTo (to, this, isAttacking = false)) {
      log.info(s"Moving $figure to $to...")

      def revertMove(): Unit = {
        to.free()
        from.place(figure)
      }
      def move(): Unit = {
        from.free()
        to.place(figure)
      }
      val figureColor = (if (figure.isWhite) "white" else "black").capitalize

      if (isInCheck)
        log.info(s"$figureColor player is in check!")

      move()

      if (isInCheck && isKingUnderAttack(figure.isWhite)) {
        log.info(s"Reverting move as $figureColor Player is in check...")
        revertMove()
        return false
      }

      if (isInCheck) {
        log.info(s"$figureColor Player is not in check anymore.")
        clearCheck()
      }

      if (isKingUnderAttack(!figure.isWhite)) {
        log.info(s"$figureColor Player: CHECK!")
        check()
      }

      true
    } else {
      log.warn(s"$figure cannot move to $to because of invalid move path!")
      false
    }
  }

  private def tryAttack(attackingFigure: Figure,
                        attackedFigure: Figure,
                        from: Square,
                        destination: Square): Boolean = {

    if (attackingFigure.isWhite == attackedFigure.isWhite) {
      log.warn("Cannot attack your own figures!")

      return false
    }

    log.info(s"Checking if $attackingFigure can attack $attackedFigure...")

    if (attackingFigure canMoveTo (destination, this, isAttacking = true)) {
      log.info(s"$attackingFigure is attacking $attackedFigure...")

      def revertMove(): Unit = {
        resurrect(attackedFigure)
        destination.free()
        destination.place(attackedFigure)
        from.place(attackingFigure)
      }
      def move(): Unit = {
        kill(attackedFigure)
        from.free()
        destination.place(attackingFigure)
      }
      val attackingFigureColor = (if (attackingFigure.isWhite) "white" else "black").capitalize
      if (isInCheck)
        log.info(s"$attackingFigureColor Player is in check!")

      move()

      if (isInCheck && isKingUnderAttack(attackingFigure.isWhite)) {
        log.info(s"Reverting move as $attackingFigureColor Player is in check...")
        revertMove()
        return false
      }

      if (isInCheck) {
        log.info(s"$attackingFigureColor player is not in check anymore.")
        clearCheck()
      }

      if (isKingUnderAttack(attackedFigure.isWhite)) {
        log.info(s"$attackingFigureColor player: CHECK!")
        check()
      }

      true
    } else {
      log.warn(s"$attackingFigure cannot attack $attackingFigure because of invalid move path!")

      false
    }
  }

  private def kill(figure: Figure): Unit = {
    log.info(s"Killing $figure and sending it to graveyard...")
    figure.kill()
    graveyard = graveyard :+ figure
  }

  private def resurrect(figure: Figure): Unit = {
    log.debug(s"Resurrecting $figure due to a rollback...")
    graveyard = graveyard.filterNot(_ == figure)
  }

  def isPathClear(from: Square, to: Square, attacking: Boolean): Boolean = {
    var rank = from.position.rank
    var file = from.position.file

    var endRank = to.position.rank
    var endFile = to.position.file

    val rankIncrement = if (rank <= endRank) 1 else -1
    val fileIncrement = if (file <= endFile) 1 else -1

    val onSameFile = file == endFile
    val onSameRank = rank == endRank
    val isDiagonal = !onSameFile && !onSameRank

    if (attacking) {
      endRank = if (onSameFile || isDiagonal) endRank - rankIncrement else endRank
      endFile = if (onSameRank || isDiagonal) endFile - fileIncrement else endFile
    }

    while (rank != endRank || file != endFile) {
      breakable {
        if (rank != endRank)
          rank = rank + rankIncrement
        if (file != endFile)
          file = file + fileIncrement

        if (matrix(rank)(file).isOccupied) return false
      }
    }

    true
  }

  private def init(): Unit = {
    initPawns()
    initRooks()
    initKnights()
    initBishops()
    initKings()
    initQueens()
  }

  private def initPawns(): Unit = {
    for (file <- 0 until Board.BOARD_DIMENSION)
      placeFigure(1, file, FigureType.PAWN, white = true)
    for (file <- 0 until Board.BOARD_DIMENSION)
      placeFigure(6, file, FigureType.PAWN, white = false)
  }

  private def initRooks(): Unit = {
    placeFigure(0, 0, FigureType.ROOK, white = true)
    placeFigure(0, 7, FigureType.ROOK, white = true)

    placeFigure(7, 0, FigureType.ROOK, white = false)
    placeFigure(7, 7, FigureType.ROOK, white = false)
  }

  private def initKnights(): Unit = {
    placeFigure(0, 1, FigureType.KNIGHT, white = true)
    placeFigure(0, 6, FigureType.KNIGHT, white = true)

    placeFigure(7, 1, FigureType.KNIGHT, white = false)
    placeFigure(7, 6, FigureType.KNIGHT, white = false)
  }

  private def initBishops(): Unit = {
    placeFigure(0, 2, FigureType.BISHOP, white = true)
    placeFigure(0, 5, FigureType.BISHOP, white = true)

    placeFigure(7, 2, FigureType.BISHOP, white = false)
    placeFigure(7, 5, FigureType.BISHOP, white = false)
  }

  private def initKings(): Unit = {
    placeFigure(0, 3, FigureType.KING, white = true)

    placeFigure(7, 3, FigureType.KING, white = false)
  }

  private def initQueens(): Unit = {
    placeFigure(0, 4, FigureType.QUEEN, white = true)

    placeFigure(7, 4, FigureType.QUEEN, white = false)
  }

  private def placeFigure(rank: Int, file: Int, figureType: FigureType, white: Boolean): Unit =
    matrix(rank)(file).place(Figure.positionedFigureFactory(figureType, white = white))

  def display(os: OutputStream): Unit = {
    val newLine = "\n"

    val stringifiedBoard = newLine + matrix.reverse
      .map(ranks => ranks.map(_.display()).mkString(" "))
      .mkString("\n") + newLine + newLine

    os.write(stringifiedBoard.getBytes())
  }

  override def toString: String =
    matrix.map(ranks => ranks.mkString(" ")).mkString("\n")
}

object Board {
  val BOARD_DIMENSION = 8
  private val buildDefaultSquareMatrix = () =>
    Matrix[Square](
      BOARD_DIMENSION,
      BOARD_DIMENSION,
      (rank: Int, file: Int) => Square(rank, file)
  )
  def apply(
      squareMatrix: Matrix[Square] = buildDefaultSquareMatrix(),
      init: Boolean = true
  ): Board = {
    val board = new Board(squareMatrix)
    if (init)
      board.init()
    board
  }
}
