package io.evilinc.chess.board

import io.evilinc.chess.figures.Figure

class Square private (val position: Position) {
  require(position != null, "Square must have a valid position!")

  var occupant: Option[Figure] = None

  def isOccupied: Boolean = occupant.isDefined
  def isEmpty: Boolean    = occupant.isEmpty

  def place(positionedFigureFactory: (Square) => Figure): Unit = {
    occupant = Some(positionedFigureFactory(this))
  }
  def place(figure: Figure): Unit = {
    occupant = Some(figure)
    figure.occupy(this)
  }

  def free(): Unit = {
    occupant = None
  }

  def display(): String = s"[${occupant.map(square => square.display).getOrElse(" ")}]"

  override def toString: String =
    s"Square(position: ${position}, occupant: ${occupant.getOrElse("vacant")})"

  def canEqual(a: Any): Boolean = a.isInstanceOf[Square]
  override def equals(that: Any): Boolean =
    that match {
      case that: Square =>
        that.canEqual(this) &&
          this.position == that.position
      case _ => false
    }
}

object Square {
  def apply(rank: Int, file: Int): Square = new Square(Position(rank, file))
}
