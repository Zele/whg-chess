package io.evilinc.chess.figures

import io.evilinc.chess.board.{Board, Square}
import io.evilinc.chess.figures.Figure.FigureType.FigureType

abstract class Figure(val isWhite: Boolean,
                      val identifier: Char,
                      var occupies: Option[Square] = None) {

  def occupy(square: Square): Unit =
    occupies = Some(square)

  def kill(): Unit = occupies = None

  def canMoveTo(square: Square, board: Board, isAttacking: Boolean): Boolean

  def display: String = {
    val startColor = if (isWhite) "" else Console.CYAN
    val resetColor = if (isWhite) "" else Console.RESET
    val id         = if (isWhite) identifier.toString else identifier.toString.toLowerCase()
    s"$startColor$id$resetColor"
  }

  override def toString: String = {
    s"Figure(identifier: $identifier, white: $isWhite})"
  }

  def canEqual(a: Any): Boolean                  = a.isInstanceOf[Figure]
  def haveSameIdentifier(other: Figure): Boolean = identifier == other.identifier
  def haveSameColor(other: Figure): Boolean      = isWhite == other.isWhite

  override def equals(that: Any): Boolean =
    that match {
      case that: Figure =>
        that.canEqual(this) &&
          haveSameIdentifier(that) &&
          haveSameColor(that)
      case _ => false
    }
}

object Figure {
  object FigureType extends Enumeration {
    type FigureType = Char
    val PAWN   = 'P'
    val ROOK   = 'R'
    val KNIGHT = 'N'
    val BISHOP = 'B'
    val KING   = 'K'
    val QUEEN  = 'Q'
  }

  private def apply(figureType: Char, white: Boolean, occupies: Option[Square]): Figure = {
    figureType match {
      case FigureType.PAWN   => new Pawn(white, occupies)
      case FigureType.ROOK   => new Rook(white, occupies)
      case FigureType.KNIGHT => new Knight(white, occupies)
      case FigureType.BISHOP => new Bishop(white, occupies)
      case FigureType.KING   => new King(white, occupies)
      case FigureType.QUEEN  => new Queen(white, occupies)
    }
  }

  def positionedFigureFactory(figureType: FigureType, white: Boolean): Square => Figure =
    (square: Square) => Figure(figureType, white, Some(square))
}
