package io.evilinc.chess.figures

import io.evilinc.chess.Logging
import io.evilinc.chess.board.{Board, Square}
import io.evilinc.chess.figures.Figure.FigureType

class King(white: Boolean, tempOccupies: Option[Square])
    extends Figure(white, FigureType.KING, tempOccupies)
    with Logging {

  override def canMoveTo(square: Square, board: Board, isAttacking: Boolean): Boolean = {
    if (occupies.isEmpty)
      return false

    val currentSquare = occupies.get

    val isPathClear = board.isPathClear(currentSquare, square, isAttacking)
    if (!isPathClear)
      return false

    val y = Math.abs(currentSquare.position.rank - square.position.rank)
    val x = Math.abs(currentSquare.position.file - square.position.file)

    val isOrthogonalMove = x + y == 1
    val isDiagonalMove   = x * y == 1

    isOrthogonalMove || isDiagonalMove
  }

}
