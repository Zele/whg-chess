package io.evilinc.chess.figures

import io.evilinc.chess.Logging
import io.evilinc.chess.board.{Board, Square}
import io.evilinc.chess.figures.Figure.FigureType

class Pawn(white: Boolean, tempOccupies: Option[Square])
    extends Figure(white, FigureType.PAWN, tempOccupies)
    with Logging {

  private var firstTurn = true

  override def canMoveTo(square: Square, board: Board, isAttacking: Boolean): Boolean = {
    if (occupies.isEmpty)
      return false

    val currentSquare = occupies.get

    val correctiveFactor = if (white) 1 else -1
    val y                = (square.position.rank - currentSquare.position.rank) * correctiveFactor
    val x                = Math.abs(currentSquare.position.file - square.position.file)

    val isForwardBy2Move = y == 2 && x == 0
    val isForwardMove    = y == 1 && x == 0
    val isCapturingMove  = x == 1 && y == 1
    val isPathClear      = firstTurn && board.isPathClear(currentSquare, square, attacking = false)

    if (isForwardBy2Move && firstTurn && isPathClear) {
      firstTurn = false
      true
    } else if (isForwardMove && square.isEmpty) {
      firstTurn = false
      true
    } else if (isCapturingMove && isAttacking)
      true
    else
      false
  }
}
