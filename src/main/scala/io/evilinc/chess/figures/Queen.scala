package io.evilinc.chess.figures

import io.evilinc.chess.Logging
import io.evilinc.chess.board.{Board, Square}
import io.evilinc.chess.figures.Figure.FigureType

class Queen(white: Boolean, tempOccupies: Option[Square])
    extends Figure(white, FigureType.QUEEN, tempOccupies)
    with Logging {

  override def canMoveTo(square: Square, board: Board, isAttacking: Boolean): Boolean = {
    if (occupies.isEmpty)
      return false

    val currentSquare = occupies.get

    val isPathClear = board.isPathClear(currentSquare, square, isAttacking)
    if (!isPathClear)
      return false

    val y = Math.abs(currentSquare.position.rank - square.position.rank)
    val x = Math.abs(currentSquare.position.file - square.position.file)

    val isHorizontalMove = y > 0 && x == 0
    val isVerticalMove   = x > 0 && y == 0
    val isDiagonalMove   = x == y

    isHorizontalMove || isVerticalMove || isDiagonalMove
  }
}
