# Chess game

Chess game built with OO & SOLID principles as a part of the technical challenge in Scala

## Documentation

Documentation as a bit scarce currently.
You can find the class diagram in the `docs/` folder which gives a good high-level overview of classes and communication.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Scala and sbt need to be installed first.

For best developer experience, IntelliJ IDE is highly recommended.

* Download Scala SDK v2.13.1 from https://www.scala-lang.org/download/

* Download sbt v1.3.4 from https://www.scala-sbt.org/download.html

* (Optional) Download IntelliJ v2019.2 from https://www.jetbrains.com/idea/download/#section=mac

### Installing

This project is built using sbt.

#### Compiling

To compile the project, run:

    $ sbt compile

#### Packaging

To create a executable jar file, run the package command:

    $ sbt package
    
The jar should be located at `target/scala-2.13/whg-chess_2.13-0.1.jar` 

#### Running

This project accepts 2 program arguments.
First one is the path to the moves file and is required.
The second one is the game name, and it is optional.

If you want to run the program from CLI, provide the path to the moves file as the first command-line argument

Example:

    $ sbt "run <path_to_moves_file> <optional_game_name>
    
## Running the tests

Unit tests are stored in `src/test/` folder.

Unit tests are written using [ScalaTest](http://www.scalatest.org/) framework.

To run unit tests, run the following command:

    $ sbt test

### And coding style tests

This project uses IntelliJ's default Scala linter, as newest version of SBT doesn't currently support existing lint plugins

## Built With

* [Scala](https://www.scala-lang.org) - Scala programming language
* [sbt](https://www.scala-sbt.org) - Scala Build Tool
* [ScalaTest](http://www.scalatest.org/) - Scala Testing framework
* [IntelliJ](https://www.jetbrains.com/idea/) - IDE

## Authors

* **Dejan Zele Pejčev** - *Initial work* - pejcev.dejan@gmail.com
